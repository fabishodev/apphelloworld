package com.example.apphelloworld.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.apphelloworld.R
import com.example.apphelloworld.databinding.ActivityHelloBinding
import com.example.apphelloworld.databinding.ActivityToolbarBinding

class HelloActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHelloBinding

    /*private lateinit var txtName:TextView
    private lateinit var btnBack:Button
    private lateinit var btnBackReturn:Button*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_hello)
        binding = ActivityHelloBinding.inflate(layoutInflater)

        /*txtName = findViewById(R.id.txt_name)
        btnBack = findViewById(R.id.btn_back)
        btnBackReturn = findViewById(R.id.btn_back_return)*/

        val name = intent.getStringExtra("name")

        var hello = getString(R.string.hello)
        binding.txtName.text = "$hello $name"

        binding.btnBack.setOnClickListener {
            finish()
        }

        binding.btnBackReturn.setOnClickListener {
            var intent = Intent()
            intent.putExtra("valor1","Test1")
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
    }
}