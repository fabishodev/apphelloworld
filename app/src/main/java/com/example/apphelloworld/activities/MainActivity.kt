package com.example.apphelloworld.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.apphelloworld.R
import com.example.apphelloworld.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    /*private lateinit var btnSayHello:Button
    private lateinit var btnSayHello2:Button
    private lateinit var btnToolbar:Button
    private lateinit var btnFragments:Button
    private lateinit var txtLabel:TextView
    private lateinit var editName:EditText
    private lateinit var btnFragments2:Button*/

    private val TAG = "TEST"
    private val TAG2 = "LifeCycle"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)

        //val btnSayHello = findViewById<Button>(R.id.btn_say_hello)
        /*btnSayHello = findViewById(R.id.btn_say_hello)
        btnSayHello2 = findViewById(R.id.btn_say_hello_2)
        btnToolbar = findViewById(R.id.btn_toolbar)
        btnFragments = findViewById(R.id.btn_fragments)
        txtLabel = findViewById(R.id.txt_label)
        editName = findViewById(R.id.edit_name)
        btnFragments2 = findViewById(R.id.btn_fragments_2)*/

        binding.btnSayHello.setOnClickListener{
            val name = binding.editName.text.toString()
            sayHello(name)
        }
        binding.btnSayHello2.setOnClickListener {
            val name = binding.editName.text.toString()
            var intent = Intent(this, HelloActivity::class.java)

            intent.putExtra("name", name)

            //startActivity(intent)
            startActivityForResult(intent, 1)
        }

        binding.btnToolbar.setOnClickListener {
            var intent = Intent(this, ToolbarActivity::class.java)
            startActivity(intent)
        }

        binding.btnFragments.setOnClickListener {
            var intent = Intent(this, FragmentsActivity::class.java)
            startActivity(intent)
        }

        binding.btnFragments2.setOnClickListener {
            var intent = Intent(this, Fragments2Activity::class.java)
            startActivity(intent)

        }

        Log.e(TAG, "This is an error")
        Log.d(TAG, "This is for debugging")
        Log.w(TAG, "This is an warning")
        Log.i(TAG, "This is information")
        Log.d(TAG2, "onCreate")

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == Activity.RESULT_OK){
            val valor1 = data?.getStringExtra("valor1")
            Toast.makeText(this, "valor1", Toast.LENGTH_LONG).show()
        }
    }

    fun sayHello(name:String){
        var hello = getString(R.string.hello)
        binding.txtLabel.text = "$hello $name"
    }

    override fun onStart(){
        super.onStart()
        Log.d(TAG2, "onStart")
    }

    override fun onResume(){
        super.onResume()
        Log.d(TAG2, "onResume")
    }

    override fun onPause(){
        super.onPause()
        Log.d(TAG2, "onPause")
    }

    override fun onStop(){
        super.onStop()
        Log.d(TAG2, "onStop")
    }

    override fun onRestart(){
        super.onRestart()
        Log.d(TAG2, "onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG2, "onDestroy")
    }

}