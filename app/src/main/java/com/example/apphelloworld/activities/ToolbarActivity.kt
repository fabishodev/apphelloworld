package com.example.apphelloworld.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.example.apphelloworld.R
import com.example.apphelloworld.databinding.ActivityMainBinding
import com.example.apphelloworld.databinding.ActivityToolbarBinding

class ToolbarActivity : AppCompatActivity() {

    private lateinit var binding: ActivityToolbarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_toolbar)

        binding = ActivityToolbarBinding.inflate(layoutInflater)

        //val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(binding.toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.mymenu,menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
            R.id.item_1 -> Toast.makeText(this, "Elemento 1", Toast.LENGTH_SHORT).show()
            R.id.item_2 -> Toast.makeText(this, "Elemento 2", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }
}