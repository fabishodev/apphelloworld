package com.example.apphelloworld.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.apphelloworld.R
import com.example.apphelloworld.databinding.FragmentMainBinding

class MainFragment : Fragment(R.layout.fragment_main) {

    private lateinit var binding: FragmentMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var nombre = "Fabricio"


        binding.btnFragmentDetail.setOnClickListener{
            /*findNavController().navigate(R.id.action_mainFragment_to_detailFragment, bundleOf(
                "nombre" to nombre
            ))*/

            /*val action = MainFragmentDirections.actionMainFragmentToDetailFragment()
            action.nombre = nombre

            findNavController().navigate(action)*/

            //findNavController().navigate(R.id.action_mainFragment_to_main2_graph)
            findNavController().navigate(Uri.parse("myapp://blank2fragment"))
        }
    }

}